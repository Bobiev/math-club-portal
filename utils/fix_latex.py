
import sys
import os
sys.path.append('..')
from app.models import Problem

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

basedir = '..'
engine = create_engine('sqlite:///' + os.path.join(basedir, 'app.db'))
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)


def fix1(p):
    text = p.text
    for from_, to in [('\\plus', '+'), ('\\minus', '-'), ('\\equal', '=')]:
        text = text.replace(from_, to)
    p.text = text


fixes = [fix1]

session = Session()
for p in session.query(Problem).all():
    print('fixing %s', p)
    for fix in fixes:
        fix(p)

session.commit()
