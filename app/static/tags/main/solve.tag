<math>
	<div ref="math_text">{ text }</div>
	<script>
		this.text = opts.text

		shouldUpdate(data, nextOpts){
			if (this.text === nextOpts.text)
				return false
			return true
		}

		update_rendered(){
			this.refs.math_text.innerHTML = xbbMarkup(this.text)
			MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.refs.math_text]);
		}
		this.on('update', function(){
			this.update_rendered()
		})

		this.on('mount', function(){
			this.update_rendered()
		})
	</script>
</math>

<editor>
	<h4> Preview </h4>
	<div id="solution_preview" class="jumbotron">
		<math text=''>
	</div>

	<button type="button" class="btn btn-default" onclick={ refreshPreview }>Refresh</button>
	<span if={ this.solution && this.solution.is_new }>
		<button type="button" class="btn btn-success" onclick={ submitNewClick }>Submit</button>
		<button type="button" class="btn btn-warning" onclick={ cancelNewClick }>Cancel</button>
	</span>
	<span if={ this.solution && !this.solution.is_new }>
		<button type="button" class="btn btn-success" onclick={ commitEditClick }>Commit</button>
		<button type="button" class="btn btn-warning" onclick={ cancelEditClick }>Cancel</button>
	</span>
	<div id="editor" style="height: 300px;"></div>

	<script>
		this.solution = opts.solution

		reset(){
			this.solution = null
			this.parent.mode = 'init'
		}

		refreshPreview(e){
			console.log("refreshPreview", this);
			this.tags.math.update({text: this.editor.session.getValue()})
		}

		cancelNewClick(e){
			if (!confirm("Are you sure you want to discard your new solution?")){
				return
			}
			this.free()
			this.parent.update()
		}

		submitNewClick(e){
			$.ajax({
				url: this.solution._links.submit_url,
				type: 'POST',
				data: {
					problem_id: this.parent.problem.id,
					text: this.editor.session.getValue(),
				}
			})
			.done(function (data, status){
				this.parent.trigger('add_new_solution', data)
				this.free()
				alert('Solution submitted')
			}.bind(this))
			.fail(function(){
				alert("Submission failed, try again.")
			})
		}

		loadSolution(solution){
			this.solution = solution
			var session = ace.createEditSession("", "ace/mode/tex")
			this.editor.setSession(session)
			this.editor.session.setValue(solution.text)
			this.refreshPreview()
		}

		cancelEditClick(e){
			if (this.solution.text != this.editor.session.getValue()
				&& !confirm('Are you sure to discard your changes?')){
					return
			}
			this.reset()
		}

		commitEditClick(e){
			if (this.solution.text == this.editor.session.getValue()){
				return
			}

			$.ajax({
				url: this.solution._links.edit_url,
				type: 'PUT',
				data: {
					text: this.editor.session.getValue()
				}
			})
			.done(function (data, status){
				this.solution = data
				this.parent.trigger('edited_solution', data)
				alert('Solution edited successfully.')
			}.bind(this))
			.fail(function(){
				alert("Editing failed, try again.")
			})
		}

		this.on('update', function(){
			console.log('<editor> updated')
		})

		this.on('mount', function() {
				console.log('editor mounting')
				var editor = ace.edit("editor")
				editor.session.setMode("ace/mode/tex")
				editor.session.setOptions({
					wrap: true,
					indentedSoftWrap: false,
				});

				if (this.solution && sessionStorage.getItem('ace_text_solution_' + this.solution.id)){
					var ace_text = sessionStorage.getItem('ace_text_solution_' + this.solution.id)
					editor.setValue(ace_text)
				}
				editor.on('change', function(){
					if (this.solution)
						sessionStorage.setItem('ace_text_solution_' + this.solution.id, editor.getSession().getValue())
				})
				this.editor = editor
			}.bind(this)
		)
	</script>
</editor>

<solution>
	<div class="solution-head">
		<span class="name">Solution { opts.index }. <span style="color: #666">{ solution.timestamp }</span> </span>
		<span class="options">
			<a hide={ solution.state == 1 } href="javascript:;" onclick={ deleteClick }>Delete</a>
			<a hide={ solution.state == 1 } href="#editor" onclick={ editClick }>Edit</a>
			<a hide={ solution.state == 1 } href="javascript:;" onclick={ stageClick }>Stage</a>
			<span show={ solution.state == 1 } class="label label-danger">Staged</span>
			<span class='label label-{success: solution.verdict==0, warning: solution.verdict==1, danger: solution.verdict==2}'>{ ['correct', 'partial', 'incorrect'][solution.verdict] }</span>
		</span>
	</div>
	<div class="jumbotron">
		<math text={ solution.text }></math>
	</div>
	<a href="javascript:void(0)" onClick={ toggleComments }>Comments </a> <sup class='label label-primary'>{ solution.grading_comments.length }</sup> 
	<div if={ show_comments } style="margin-bottom: 30px; margin-left: 50px;">
		<ul>
			<li each={ solution.grading_comments }>{ text } <span class='date'>{ timestamp }</span></li>
		</ul>
	</div>

	<script>
		deleteClick(e){
			e.preventUpdate = true
			if ( !confirm("Are you sure you want to delete Solution " + opts.index + '?') ){
				return
			}
			$.ajax({
				url: this.solution._links.del_url,
				type: 'DELETE'
			})
			.done(function (data, status){
				this.parent.trigger('delete_solution', this.solution)
				alert('Solution deleted')
			}.bind(this))
			.fail(function(){
				alert("Delete failed, try again.")
			})
		}
		editClick(e){
			e.preventUpdate = true
			if (this.solution.verdict !== null && !confirm("Editing evaluated solution removes the evaluation mark. Are you sure you want to edit Solution " + opts.index + '?'))
				return 
			this.parent.trigger('edit_solution', this.solution)
		}

		stageClick(e){
			e.preventUpdate = true
			if ( !confirm("Are you sure you want to stage the Solution " + this.opts.index 
				+ '?\nYou may not be able to edit or stage it back until it is evaluated.') ){
				return
			}
			$.ajax({
				url: this.solution._links.stage_url,
				type: 'PUT',
			})
			.done(function (data, status){
				this.solution.state = 1
				this.update()
			}.bind(this))
			.fail(function(){
				alert("Staging failed, try again.")
			})
		}

		toggleComments(e){
			this.show_comments = !this.show_comments
		}
	</script>
</solution>

<solve>
	<input type="hidden" name="text">
	<h4>Problem: { problem.source } </h4>
	<div class="jumbotron"> 
		<math text={ problem.text }></math>
	</div>

	<h4> Solutions </h4>
	<div>
		<solution each={ solution, i in solutions } index={ i+1 } id="solution_{solution.id}"></solution>
	</div> 

	<button show={ this.mode == 'init' } ref="newSolutionButton" class="btn btn-success" onclick={ newSolutionClick }>New Solution</button>

	<editor show={ this.mode == 'new' || this.mode == 'edit' }></editor>

	<script>
		this.solutions = opts.solutions
		this.problem = opts.problem
		this.mode = 'init'

		newSolutionClick(e){
			this.new_solution = {
				text: '',
				is_edited: true,
				is_new: true,
				_links: {
					submit_url: pageData.submit_url
				}
			}
			this.tags.editor.update({solution: this.new_solution})
			this.mode = 'new'
			this.trigger('mode', 'new')
		}

		this.on('edit_solution', function(solution){
			this.tags.editor.loadSolution(solution)
			this.mode = 'edit'
			this.update()
		})

		this.on('edited_solution', function(solution){
			for (i = 0; i < this.solutions.length; i++){
				if (this.solutions[i].id == solution.id){
					this.solutions[i] = solution
					break
				}
			}
			this.mode = 'init'
			this.update()
		})

		this.on('add_new_solution', function(solution){
			this.solutions.push(solution)
			this.mode = 'init'
			this.update()
		})

		this.on('delete_solution', function(solution){
			var index = this.solutions.indexOf(solution)
			this.solutions.splice(index, 1)
			this.update()
		})

		this.on('mode', function(mode){
		})

		this.on('update', function() {
			console.log('<solve> updated')
		})
	</script>
</solve>
