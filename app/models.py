import base64
from datetime import datetime, timedelta
import os
from hashlib import md5
from app import login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db
from flask import url_for


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


followers = db.Table('followers',
                     db.Column('follower_id', db.Integer,
                               db.ForeignKey('user.id')),
                     db.Column('followed_id', db.Integer,
                               db.ForeignKey('user.id'))
                     )


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    name = db.Column(db.String(64))
    surname = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    solutions = db.relationship('Solution', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    rights = db.Column(db.Text(), default='')

    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def can(self, right):
        return self.rights and (right in self.rights)

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'last_seen': self.last_seen,
            'about_me': self.about_me,
            'follower_count': self.followers.count(),
            'followed_count': self.followed.count(),
            '_links': {
                'self': url_for('api.get_user', id=self.id),
                'followers': url_for('api.get_followers', id=self.id),
                'followed': url_for('api.get_followed', id=self.id),
                'avatar': self.avatar(128)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)


class Problem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text())
    source = db.Column(db.String(140))
    tags = db.Column(db.String(140))
    difficulty = db.Column(db.Integer, default=1)
    collection_id = db.Column(db.Integer, db.ForeignKey('collection.id'))
    solutions = db.relationship('Solution', backref='problem', lazy='dynamic')

    def to_dict(self):
        data = {
            'id': self.id,
            'text': self.text,
            'source': self.source,
            'tags': self.tags,
            'difficulty': self.difficulty,
            'collection_id': self.collection_id
        }
        return data


class Solution(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.Text())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'))
    # 0 - correct, 1 - improve it, 2 - incorrect
    verdict = db.Column(db.Integer)
    state = db.Column(db.Integer)  # 1 - staged
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    modified_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    state_timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    grading_comments = db.relationship(
        'GradingComment', backref='solution', lazy='dynamic')

    def to_dict(self, include_grading_comments=False):
        data = {
            'id': self.id,
            'text': self.text,
            'user_id': self.user_id,
            'problem_id': self.problem_id,
            'verdict': self.verdict,
            'state': self.state,
            'state_timestamp': self.state_timestamp,
            'timestamp': self.timestamp,
            'modified_date': self.modified_date
        }
        if include_grading_comments:
            data['grading_comments'] = [gc.to_dict()
                                        for gc in self.grading_comments]
        return data

    def stage(self, news=True):
        if self.state == 1:
            raise ValueError("Solution already staged!")
        self.state = 1
        self.state_timestamp = datetime.utcnow()

        if news:
            # check if news exists remove it
            vn = VerdictNews.query.filter_by(solution_id=self.id).first()
            if vn:
                db.session.delete(vn)

    def unstage(self, news=True):
        if self.state != 1:
            raise ValueError('Solution state != staged!')
        self.state = 0
        self.state_timestamp = datetime.utcnow()

        if news:
            # check if news exists
            vn = VerdictNews.query.filter_by(solution_id=self.id).first()
            if vn is not None:
                vn.verdict = self.verdict
                vn.timestamp = datetime.utcnow()
            else:
                vn = VerdictNews(solution_id=self.id, verdict=self.verdict)
                db.session.add(vn)


class Collection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(140))
    parent_id = db.Column(db.Integer, db.ForeignKey('collection.id'))
    problems = db.relationship('Problem', backref='collection', lazy='dynamic')
    children = db.relationship('Collection',
                               backref=db.backref('parent', remote_side=[id]),
                               lazy='dynamic')

    def to_dict(self):
        data = {
            'id': self.id,
            'name': self.name,
            'parent_id': self.parent_id,
        }
        return data


class GradingComment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    solution_id = db.Column(db.Integer, db.ForeignKey('solution.id'))
    text = db.Column(db.Text())
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def to_dict(self):
        data = {
            'id': self.id,
            'user_id': self.user_id,
            'solution_id': self.solution_id,
            'text': self.text,
            'timestamp': self.timestamp
        }
        return data


class VerdictNews(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    solution_id = db.Column(db.Integer, db.ForeignKey('solution.id'))
    verdict = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def to_dict(self):
        solution = Solution.query.get(self.solution_id)
        problem = Problem.query.get(solution.problem_id)
        user = User.query.get(solution.user_id)
        data = {
            'timestamp': self.timestamp,
            'verdict': self.verdict,
            'problem_source': problem.source,
            'username': user.username,
            '_links': {
                'problem': url_for('main.solve', problem_id=solution.problem_id),
                'solution': url_for('main.solve', problem_id=solution.problem_id) + '#solution_%s'%self.solution_id,
                'user': url_for('main.user', username=user.username),
            }
        }
        return data
