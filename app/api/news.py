from flask import url_for, jsonify, request, current_app
from flask_login import current_user
from app import db
from app.api.errors import bad_request, error_response
from app.models import User, VerdictNews, Solution
from app.api import bp
from app.api.auth import token_auth


@bp.route('/news/verdict', methods=['GET'])
@token_auth.login_required
def verdict_news():
    page = request.args.get('page', 1, type=int)
    verdict_news = VerdictNews.query.order_by(VerdictNews.timestamp.desc())\
            .paginate(page, current_app.config['VERDICT_NEWS_PER_PAGE'], False)
    next_url = url_for('api.verdict_news', page=verdict_news.next_num) \
        if verdict_news.has_next else None

    items = [v.to_dict() for v in verdict_news.items]
    data = {'items': items, 'next_url': next_url}
    return jsonify(data)


@bp.route('/news/recent-solutions/<int:user_id>', methods=['GET'])
@token_auth.login_required
def recent_solutions_news(user_id):
    if current_user.id != user_id:
        return error_response(403, 'No permission.')
    user = User.query.get_or_404(user_id)
    if user is None:
        return bad_request('User not found.')

    page = request.args.get('page', 1, type=int)
    solutions = Solution.query.filter_by(user_id=user_id).order_by(Solution.modified_date.desc())\
            .paginate(page, current_app.config['RECENT_SOLUTIONS_PER_PAGE'], False)
    next_url = url_for('api.recent_solutions_news', user_id=user_id, page=solutions.next_num) \
        if solutions.has_next else None

    items = []
    for sol in solutions.items:
        item = {
            'problem_source': sol.problem.source,
            'solution_id': sol.id,
            'verdict': sol.verdict,
            'state': sol.state,
            'modified_date': sol.modified_date,
            '_links': {
                'solution': url_for('main.solve', problem_id=sol.problem_id),
            },
        }
        items.append(item)
    print(items[0]['modified_date'])
    data = {'items': items, 'next_url': next_url}
    return jsonify(data)
