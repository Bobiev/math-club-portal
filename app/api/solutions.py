from datetime import datetime
from flask import url_for, jsonify, request
from flask_login import current_user
from app import db
from app.api.errors import bad_request, error_response
from app.models import Solution, GradingComment, VerdictNews
from app.api import bp
from app.api.auth import token_auth


def make_solution_links(solution):
    if isinstance(solution, dict):
        id = solution['id']
    else:
        id = solution.id
    return {
        'del_url': url_for('api.del_solution', id=id),
        'edit_url': url_for('api.edit_solution', id=id),
        'stage_url': url_for('api.stage_solution', id=id),
    }


@bp.route('/solutions/<int:id>', methods=['GET'])
@token_auth.login_required
def get_solution(id):
    return jsonify(Solution.query.get_or_404(id).to_dict())


@bp.route('/solution/<int:id>', methods=['DELETE'])
@token_auth.login_required
def del_solution(id):
    solution = Solution.query.get_or_404(id)
    if solution.user_id != current_user.id:
        return bad_request('Not solution author.')
    if solution.state == 1:  # staged
        return error_response(403, "Cannot delete staged solution.")
    db.session.delete(solution)

    # delete from VerdictNews if exists
    vn = VerdictNews.query.filter_by(solution_id=id).first()
    if vn is not None:
        db.session.delete(vn)

    db.session.commit()
    return jsonify(message='Deleted successfully')


@bp.route('/solutions', methods=['GET'])
@token_auth.login_required
def get_solutions():
    solutions = Solution.query.all().order_by(Solution.timestamp.desc())
    items = [solution.to_dict() for solution in solutions]
    data = {
        'items': items
    }
    return jsonify(data)


@bp.route('/solutions/judge/<int:id>', methods=['POST'])
@token_auth.login_required
def judge_solution(id):
    if not current_user.can('judge_solution'):
        return error_response(403, 'Missing privilege: judge_solution')
    solution = Solution.query.get_or_404(id)
    if 'verdict' in request.form:
        solution.verdict = request.form['verdict']
        db.session.commit()
        return jsonify(verdict=solution.verdict)
    elif 'comment' in request.form:
        text = request.form.get('comment')
        if not text:
            return bad_request('no comment text')
        user_id = current_user.id
        comment = GradingComment(text=text, user_id=user_id, solution_id=id)
        db.session.add(comment)
        db.session.commit()
        return jsonify(comment.to_dict())
    elif 'delete_comment_id' in request.form:
        comment_id = request.form['delete_comment_id']
        comment = GradingComment.query.get_or_404(comment_id)
        db.session.delete(comment)
        db.session.commit()
        return jsonify()
    elif request.form.get('a') == 'unstage':
        solution.unstage()  # remove stage
        db.session.commit()
        return jsonify()


@bp.route('/solutions', methods=['POST'])
@token_auth.login_required
def add_solution():
    text = request.form.get('text')
    if not text:
        return bad_request('No text')
    problem_id = request.form.get('problem_id')
    if not problem_id:
        return bad_request('No problem_id')
    user_id = current_user.id
    solution = Solution(text=text, problem_id=problem_id, user_id=user_id)
    db.session.add(solution)
    db.session.commit()
    sol_dict = solution.to_dict()
    sol_dict['_links'] = make_solution_links(solution)
    return (jsonify(sol_dict), 201)


@bp.route('/solutions/<int:id>', methods=['PUT'])
@token_auth.login_required
def edit_solution(id):
    solution = Solution.query.get_or_404(id)

    if solution.user_id != current_user.id:
        return error_response(403, 'Not solution author.')

    if solution.state == 1:  # staged
        return bad_request("Cannot edit staged solution.")

    text = request.form.get('text')
    if not text:
        return bad_request('No text')

    solution.verdict = None
    solution.text = text
    solution.modified_date = datetime.utcnow()
    db.session.commit()
    sol_dict = solution.to_dict()
    sol_dict['_links'] = make_solution_links(solution)
    return jsonify(sol_dict)
    # TODO: modified date


@bp.route('/solutions/stage/<int:id>', methods=['PUT'])
@token_auth.login_required
def stage_solution(id):
    solution = Solution.query.get_or_404(id)

    if solution.user_id != current_user.id:
        return error_response(403, 'Not solution author.')
    solution.stage()  # staged
    db.session.commit()
    return jsonify()
