from flask import (
    render_template, url_for, request, jsonify
)
from flask_login import login_required, current_user
from app import db
from app.admin import bp
from app.models import User, Collection, Problem, Solution
from app.post_fetcher import PostFetcher


@bp.route('/')
@login_required
def admin():
    return render_template("admin/admin.html")


@bp.route('/problem', methods=['GET', 'POST'])
@login_required
def problem():
    if not current_user.can('manage_problems'):
        return "You can not manage problems", 404
    if request.method == 'GET':
        if request.args.get('action') == 'delete':
            id = request.args.get('problem_id')
            problem = Problem.query.get(id)
            if not problem:
                message = 'Problem not found, did you modify Problem ID?'
                return jsonify(status='fail', message=message)
            db.session.delete(problem)
            db.session.commit()
            return jsonify(status='success', message='Deleted')
        else:
            problem_id = request.args.get("problem_id")
            if problem_id:
                problem = Problem.query.get(problem_id)
                if not problem:
                    return jsonify(status="fail", message="Problem not found")
                return jsonify(status='success', problem=problem.to_dict())
            pageData = {'collections': [coll.to_dict()
                                        for coll in Collection.query.all()]}
            return render_template("admin/admin_problem.html",
                                   pageData=pageData)

    elif request.method == 'POST':
        f = request.form
        print(f)
        id = f.get('id')
        text = f.get('text')
        difficulty = f.get('difficulty')
        source = f.get('source')
        collection_id = f.get('collection_id')
        mode = f.get('mode')
        if not text:
            return jsonify(status='fail', message='No text')
        if not id and mode == 'edit':
            return jsonify(status='fail', message='No problem ID')
        if not difficulty or not difficulty.isnumeric():
            return jsonify(status='fail', message='No difficulty')
        if not collection_id:
            return jsonify(status='fail', message='No collection')
        if mode == 'edit':
            problem = Problem.query.get(id)
            if not problem:
                message = 'Problem not found, did you modify Problem ID?'
                return jsonify(status='fail', message=message)
            problem.text = text
            problem.difficulty = difficulty
            problem.source = source
            problem.collection_id = collection_id
            db.session.commit()
            return jsonify(status='success', message='Saved')
        elif mode == 'new':
            try:
                problem = Problem(
                    text=text, collection_id=collection_id,
                    source=source, difficulty=difficulty)
                db.session.add(problem)
                db.session.commit()
                return jsonify(status='success', message='Created',
                               problem_id=problem.id)
            except Exception as e:
                return jsonify(status='fail', message=str(e))


@bp.route('/collection')
@login_required
def collection():
    if not current_user.can('manage_collections'):
        return "You can not manage collections.", 403
    return render_template("admin/admin_collection.html")


@bp.route('/aops-scrape')
@login_required
def aops_scrape():
    aops_url = request.args.get("aops_url")
    pf = PostFetcher()
    posts = pf.fetch(aops_url)
    problem_text = posts[0]
    return jsonify(status='success', problem_text=problem_text)


@bp.route('/solution')
@login_required
def solution():
    if not current_user.can('judge_solutions'):
        return 'You can not judge solutions.', 403
    items = []
    # query all solutions that are staged (solution.state = 1)
    for sol in Solution.query.filter_by(state=1).order_by(Solution.state_timestamp.desc()).all():
        item = {}
        item['solution'] = sol.to_dict()
        item['solution']['_links'] = {
            'judge_url': url_for('api.judge_solution', id=sol.id)}
        item['user'] = User.query.get(sol.user_id).to_dict()
        item['problem'] = Problem.query.get(sol.problem_id).to_dict()
        item['grading_comments'] = [gc.to_dict()
                                    for gc in sol.grading_comments]
        items.append(item)
    pageData = {'items': items}
    return render_template("admin/admin_solution.html", pageData=pageData,
                           tag_path=tag_path)


def tag_path(filename):
    return '/static/tags/admin/%s' % filename
