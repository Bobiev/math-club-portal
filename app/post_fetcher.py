import json
import re
import requests


class PostFetcher():
    pat = re.compile(r'post_canonical":(.*?),"username"')

    def fetch(self, url):
        '''Retrieve posts from url. Return [post: string]'''
        text = requests.get(url).text
        posts = self.pat.findall(text)
        return [json.loads(post) for post in posts]


def test():
    pf = PostFetcher()
    url = "https://artofproblemsolving.com/community/c6h1671293p10632360"
    posts = pf.fetch(url)
    print(posts[0])


if __name__ == '__main__':
    test()
