from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_required
from app import db
from app.main.forms import EditProfileForm
from app.models import User, Problem, Solution, Collection
from app.main import bp
from datetime import datetime
import app.api as api


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@bp.route('/')
@bp.route('/index')
@login_required
def index():
    verdict_news_tag_url = '/static/tags/main/verdict_news_tag.html'
    recent_solutions_news_tag_url = '/static/tags/main/recent_solutions_news_tag.html'
    pageData = {
        'verdict_news_url': url_for('api.verdict_news'),
        'recent_solutions_news_url': url_for('api.recent_solutions_news', user_id=current_user.id),
        'user': current_user.to_dict(),
    }
    return render_template('index.html', title='Home', 
                           verdict_news_tag_url=verdict_news_tag_url,
                           recent_solutions_news_tag_url=recent_solutions_news_tag_url,
                           pageData=pageData)


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    print( user.last_seen )
    return render_template('user.html', user=user, title=username)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.name.data = current_user.name
        form.surname.data = current_user.surname
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('You cannot follow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('You are following {}!'.format(username))
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('You cannot unfollow yourself!')
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    return redirect(url_for('main.user', username=username))


@bp.route('/collections/')
@bp.route('/collections/<int:collection_id>')
def collections(collection_id=None):
    if not collection_id:
        collection = Collection.query.filter_by(parent_id=None).first()
        collection_id = collection.id
    else:
        collection = Collection.query.get(collection_id)

    def do(col):
        res = {"name": col.name, "id": col.id, "children": []}
        for child in col.children:
            res["children"].append(do(child))
        return res
    collection = do(collection)

    problems = Problem.query.filter_by(collection_id=collection_id)
    problems = [{c.name: getattr(problem, c.name)
                 for c in problem.__table__.columns} for problem in problems]
    for problem in problems:
        problem['url'] = url_for('main.solve', problem_id=problem['id'])
    pageData = {'problems': problems}
    return render_template('collections.html', collection=collection,
                           pageData=pageData, title='Collections')


@bp.route('/solve/<int:problem_id>')
@login_required
def solve(problem_id):
    problem = Problem.query.get(problem_id)
    solutions = Solution.query.filter_by(
        user_id=current_user.id, problem_id=problem_id).order_by(Solution.timestamp.asc())
    solutions_dicted = [sol.to_dict(
        include_grading_comments=True) for sol in solutions]
    for item in solutions_dicted:
        item['_links'] = api.solutions.make_solution_links(item)

    pageData = {'solutions': solutions_dicted, 'problem': problem.to_dict(
    ), 'submit_url': url_for('api.add_solution')}
    print(pageData)
    return render_template("solve.html", pageData=pageData, tag_path=tag_path,
                           title='Solve')


def tag_path(filename):
    return '/static/tags/main/%s' % filename
